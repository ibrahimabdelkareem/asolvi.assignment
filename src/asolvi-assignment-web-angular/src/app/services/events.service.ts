import { Injectable } from '@angular/core';
import { Event } from '../models/event'
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { MessagingService } from './messaging.service'
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private resourceUrl: string;

  constructor(private httpClient: HttpClient, private messagingService: MessagingService) {
    this.resourceUrl = "http://localhost:55691/api/events";
  }

  getAll(): Observable<Event[]> {
    return this.httpClient.get<Event[]>(this.resourceUrl);
  }

  get(id: string): Observable<Event> {
    const url = `${this.resourceUrl}/${id}`;
    return this.httpClient.get<Event>(url, httpOptions);
  }

  create(event: Event): Observable<Event> {
    return this.httpClient.post<Event>(this.resourceUrl, event, httpOptions);
  }

  update(event: Event): Observable<Event> {
    const url = `${this.resourceUrl}/${event.id}`;
    return this.httpClient.put<Event>(url, event, httpOptions);
  }

  delete(id: string): Observable<Event> {
    const url = `${this.resourceUrl}/${id}`;

    return this.httpClient.delete<Event>(url, httpOptions);
  }

}
