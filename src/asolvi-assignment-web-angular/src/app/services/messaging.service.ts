import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class MessagingService {

  constructor(private snackBar:MatSnackBar) {
  }

  error(message: string): void {
    this.snackBar.open(message);
  };

  warning(message: string): void {
    this.snackBar.open(message);
  }

  success(message: string) {
    this.snackBar.open(message);
  }
}
