import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'  
import { AppMaterialModule } from './modules/material/app-material.module'

import { AppComponent } from './app.component';
import { EventsComponent } from './components/events/events.component';
import { AppRoutingModule } from './modules/routing/app-routing.module';
import { CreateEventComponent } from './components/events/create-event.component';
import { EventDetailsComponent } from './components/events/event-details.component';
import { EditEventComponent } from './components/events/edit-event.component';
import { ConfirmationDialogComponent } from "./components/dialog/confimation-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    CreateEventComponent,
    EventDetailsComponent,
    EditEventComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppMaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[ConfirmationDialogComponent]
})
export class AppModule { }
