import { NgModule } from '@angular/core';

import {
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatTableModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatCardModule,
  MatSidenavModule,
  MatNativeDateModule,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher,
  MatToolbarModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';


@NgModule({
  imports: [MatDatepickerModule, MatFormFieldModule, MatInputModule,
    MatButtonModule, MatIconModule, MatProgressSpinnerModule,
    MatDialogModule, MatSnackBarModule, MatTooltipModule,
    MatTableModule, MatCardModule, MatSidenavModule,
    MatNativeDateModule, CdkTableModule, MatToolbarModule],
  exports: [MatDatepickerModule, MatFormFieldModule, MatInputModule,
    MatButtonModule, MatIconModule, MatProgressSpinnerModule,
    MatDialogModule, MatSnackBarModule, MatTooltipModule,
    MatTableModule, MatCardModule, MatSidenavModule,
    MatNativeDateModule, CdkTableModule, MatToolbarModule],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2000 } }
  ]
})

export class AppMaterialModule { }
