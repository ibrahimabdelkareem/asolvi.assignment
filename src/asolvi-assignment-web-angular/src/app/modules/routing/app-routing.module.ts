import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { EventsComponent } from '../../components/events/events.component'
import { EventDetailsComponent } from '../../components/events/event-details.component'
import { CreateEventComponent } from '../../components/events/create-event.component'
import { EditEventComponent } from '../../components/events/edit-event.component'

const routes: Routes = [
  { path: '', redirectTo: "/events", pathMatch: 'full' },
  { path: 'events', component: EventsComponent },
  { path: 'events/create', component: CreateEventComponent },
  { path: 'events/edit/:id', component: EditEventComponent },
  { path: 'events/details/:id', component: EventDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
