export class Event {
  id?: string;
  title?: string;
  description?: string;
  address?: string;
  date?: Date;
  ticketPrice?: number;
}
