import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Event } from '../../models/event';
import { EventsService } from '../../services/events.service'
import { Observable, of } from 'rxjs';
import { MatTableDataSource, MatSort } from '@angular/material';
import { DataSource } from '@angular/cdk/table';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmationDialogComponent } from "../dialog/confimation-dialog.component";
import { MessagingService } from '../../services/messaging.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html'
})

export class EventsComponent implements OnInit {
  events:Array<Event>;
  eventsDataSource:MatTableDataSource<Event>;
  displayedColumns:Array<string>;
  deleteConfirmationDialogRef: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private eventsService: EventsService,
    private dialog: MatDialog,
    private changeDecoratorRef: ChangeDetectorRef,
    private messagingService: MessagingService) { }

  ngOnInit() {
    this.getAll();
    this.displayedColumns = ['title', 'date', 'ticketPrice', 'edit', 'delete'];
  }

  getAll(): void {
    this.eventsService.getAll().subscribe(events => {
      this.events = events;
      this.eventsDataSource = new MatTableDataSource(events);
    });

    
  }

  delete(event: Event): void {
    this.deleteConfirmationDialogRef = this.dialog.open(ConfirmationDialogComponent);
    this.deleteConfirmationDialogRef.componentInstance.confirmationMessage = `Are you sure you want to delete the event '${event.title}'?`;
    this.deleteConfirmationDialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.eventsService.delete(event.id).subscribe(() => {
          var index = this.events.indexOf(event);

          if (index > -1) {
            this.events.splice(index, 1);
            this.eventsDataSource = new MatTableDataSource(this.events);
            this.changeDecoratorRef.detectChanges();
          }
        });
        this.deleteConfirmationDialogRef = null;
        this.messagingService.success("Event Deleted Successfully!");
      }
    })
    
  }

}

