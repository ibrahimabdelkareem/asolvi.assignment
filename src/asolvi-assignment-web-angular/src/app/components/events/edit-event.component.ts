import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../../models/event'
import { EventsService } from '../../services/events.service'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MessagingService } from '../../services/messaging.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './create-event.component.html'
})

export class EditEventComponent implements OnInit {
  @Input() event: Event;
  eventForm: FormGroup;
  minDate: Date;
  pageTitle: string;
  mode: string;

  constructor(private eventsService: EventsService,
    private location: Location,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private matcher: ErrorStateMatcher,
    private messagingService: MessagingService) {
    this.eventForm = this.formBuilder.group({
      'title': this.formBuilder.control('', [Validators.required]),
      'description': this.formBuilder.control(''),
      'address': this.formBuilder.control('', [Validators.required]),
      'date': this.formBuilder.control('', [Validators.required]),
      'ticketPrice': this.formBuilder.control('', [Validators.min(0)])
    });

    this.minDate = new Date();
    this.pageTitle = "Edit Event";
    this.mode = "edit";
  }

  ngOnInit() {
    this.getEvent();
  }

  getEvent(): void {
    this.eventsService.get(this.route.snapshot.paramMap.get("id")).subscribe(event => {
      this.event = event;
      this.resetForm();
    });
  }

  onSubmit(): void {
    if (!this.eventForm.valid)
      return;

    var event: Event = {
      id: this.event.id,
      title: this.eventForm.get("title").value,
      address: this.eventForm.get("address").value,
      description: this.eventForm.get("description").value,
      date: this.eventForm.get("date").value,
      ticketPrice: this.eventForm.get("ticketPrice").value
    };
    this.eventsService.update(event).subscribe(() => {
      this.goBack();
      this.messagingService.success("Event Edited Successfully!");
    });
  }

  goBack(): void {
    this.location.back();
  }

  resetForm() {
    this.eventForm.get("title").setValue(this.event.title);
    this.eventForm.get("address").setValue(this.event.address);
    this.eventForm.get("description").setValue(this.event.description);
    this.eventForm.get("date").setValue(this.event.date);
    this.eventForm.get("ticketPrice").setValue(this.event.ticketPrice);
  }
}
