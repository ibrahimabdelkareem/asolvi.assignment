import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../../models/event'
import { EventsService } from '../../services/events.service'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { FormControl, FormGroup, FormBuilder, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MessagingService } from '../../services/messaging.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html'
})

export class CreateEventComponent implements OnInit {
  @Input() event: Event;
  eventForm: FormGroup;
  minDate: Date;
  pageTitle: string;
  mode: string;

  constructor(private eventsService: EventsService,
    private location: Location,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private matcher: ErrorStateMatcher,
    private messagingService: MessagingService) {
    this.eventForm = this.formBuilder.group({
      'title': this.formBuilder.control('', [Validators.required]),
      'description': this.formBuilder.control(''),
      'address': this.formBuilder.control('', [Validators.required]),
      'date': this.formBuilder.control('', [Validators.required]),
      'ticketPrice': this.formBuilder.control('', [Validators.min(0)])
    });

    this.minDate = new Date();
    this.pageTitle = "Create Event";
    this.mode = "create";
  }

  ngOnInit() {
  }

  onSubmit(): void {
    if (!this.eventForm.valid)
      return;

    var event: Event = {
      title: this.eventForm.get("title").value,
      address: this.eventForm.get("address").value,
      description: this.eventForm.get("description").value,
      date: this.eventForm.get("date").value,
      ticketPrice: this.eventForm.get("ticketPrice").value
    };
    this.eventsService.create(event).subscribe(() => {
      this.messagingService.success("Event Created Successfully!");
      this.goBack()
    });
  }

  goBack(): void {
    this.location.back();
  }
}
