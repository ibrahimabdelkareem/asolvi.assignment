import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import { Event } from '../../models/event'
import { EventsService } from '../../services/events.service'

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html'
})
export class EventDetailsComponent implements OnInit {

  event:Event;

  constructor(private eventsService:EventsService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.getEvent();
  }

  getEvent(): void {
    this.eventsService.get(this.route.snapshot.paramMap.get('id')).subscribe(event=> this.event = event);
  }

}
