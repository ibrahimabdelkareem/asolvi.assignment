﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.InMemory.Services.Repositories;
using Asolvi.Assignment.Test.Helper.TestsData;
using Xunit;

namespace Asolvi.Assignment.Data.InMemory.UnitTests.Services.Repositories
{
    public class InMemoryEventsRepositoryTests
    {
        [Fact]
        public void Ctor_WhenDictionaryArgumentIsNull_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new InMemoryEventsRepository(null));
        }

        [Theory]
        [ClassData(typeof(EventsListDataSource))]
        public async Task GetAll_WhenExecuted_ShouldReturnListOfAllEvents(IList<Event> events)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            foreach (var @event in events)
                cache.TryAdd(@event.Id, @event);

            var sut = new InMemoryEventsRepository(cache);
            var result = await sut.GetAll();
            Assert.Equal(cache.Count, result.Count);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Get_WhenFindsEventWithIdArgument_ShouldReturnEvent(Event @event)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            cache.TryAdd(@event.Id, @event);

            var sut = new InMemoryEventsRepository(cache);
            var result = await sut.Get(@event.Id);
            Assert.NotNull(result);
            Assert.Same(@event,result);
        }

        [Theory]
        [ClassData(typeof(GuidDataSource))]
        public async Task Get_WhenEventNotFound_ShouldReturnNull(Guid id)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            var sut = new InMemoryEventsRepository(cache);
            var result = await sut.Get(id);
            Assert.Null(result);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Add_ShouldAddItemToCache(Event @event)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            var sut = new InMemoryEventsRepository(cache);
            await sut.Add(@event);
            var foundInCache = cache.TryGetValue(@event.Id, out var result);
            Assert.True(foundInCache);
            Assert.Same(@event, result);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Update_WhenEventFound_ShouldUpdateEvent(Event @event)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            cache.TryAdd(@event.Id, new Event(){Id = @event.Id});
            var sut = new InMemoryEventsRepository(cache);
            await sut.Update(@event);
            var foundInCache = cache.TryGetValue(@event.Id, out var result);
            Assert.True(foundInCache);
            Assert.Same(@event, result);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Update_WhenEventNotFound_ShouldNotAddEvent(Event @event)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            var sut = new InMemoryEventsRepository(cache);
            await sut.Update(@event);
            var foundInCache = cache.TryGetValue(@event.Id, out _);
            Assert.False(foundInCache);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Delete_WhenEventFound_ShouldDeleteFromCache(Event @event)
        {
            var cache = new ConcurrentDictionary<Guid, Event>();
            cache.TryAdd(@event.Id, @event);
            var sut = new InMemoryEventsRepository(cache);
            await sut.Delete(@event.Id);
            var foundInCache = cache.TryGetValue(@event.Id, out _);
            Assert.False(foundInCache);
        }
    }
}
