﻿using System;
using System.Collections.Generic;
using System.Text;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Json.Services.Resolvers;
using Xunit;

namespace Asolvi.Assignment.Data.Json.UnitTests.Services.Resolvers
{
    public class EntityJsonFilePathResolverTests
    {
        [Fact]
        public void Ctor_WhenFolderPathArgumentIsNullOrEmpty_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new EntityJsonFilePathResolver(null));
            Assert.Throws<ArgumentNullException>(() => new EntityJsonFilePathResolver(string.Empty));
        }

        [Fact]
        public void Resolve_WhenTypeArgumentIsNull_ShouldThrowException()
        {
            const string mockFolder = "/mock-folder/";
            var sut = new EntityJsonFilePathResolver(mockFolder);
            Assert.Throws<ArgumentNullException>(() => sut.Resolve(null));
        }

        [Theory]
        [InlineData(typeof(Event)), InlineData(typeof(DateTime))]
        public void Resolve_WhenTypeArgumentIsNotNull_ShouldCombineFolderPathAndTypeName(Type type)
        {
            const string mockFolder = "/mock-folder/";
            var sut = new EntityJsonFilePathResolver(mockFolder);
            var result = sut.Resolve(type);
            Assert.Equal(string.Concat(mockFolder, type.Name, ".json"), result);
        }
    }
}
