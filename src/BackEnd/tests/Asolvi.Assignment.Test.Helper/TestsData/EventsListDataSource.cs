﻿using System.Collections;
using System.Collections.Generic;
using Asolvi.Assignment.Test.Helper.Generators;

namespace Asolvi.Assignment.Test.Helper.TestsData
{
    public class EventsListDataSource : IEnumerable<object[]>
    {
        private readonly List<object[]> _data;

        public EventsListDataSource()
        {
            _data = new List<object[]>
            {
                new object[]{MockDataGenerator.GenerateList(MockDataGenerator.GenerateEvent, 10)},
                new object[]{MockDataGenerator.GenerateList(MockDataGenerator.GenerateEvent, 7)}
            };
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
