﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Asolvi.Assignment.Test.Helper.TestsData
{
    public class GuidDataSource : IEnumerable<object[]>
    {
        private readonly List<object[]> _data;

        public GuidDataSource()
        {
            _data = new List<object[]>();
            for(var c = 0 ; c < 5 ; c++)
                _data.Add(new object[]{Guid.NewGuid()});
        }
        public IEnumerator<object[]> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
