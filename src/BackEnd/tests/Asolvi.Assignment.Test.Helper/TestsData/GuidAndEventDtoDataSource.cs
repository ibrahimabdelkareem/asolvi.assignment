﻿using System;
using System.Collections;
using System.Collections.Generic;
using Asolvi.Assignment.Test.Helper.Generators;

namespace Asolvi.Assignment.Test.Helper.TestsData
{
    public class GuidAndEventDtoDataSource : IEnumerable<object[]>
    {
        private readonly List<object[]> _data;

        public GuidAndEventDtoDataSource()
        {
            _data = new List<object[]>();
            for(var c = 0 ; c < 5 ; c++)
                _data.Add(new object[]{Guid.NewGuid(), MockDataGenerator.GenerateEventDto()});
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
