﻿using System;
using System.Collections.Generic;
using System.Text;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Web.Api.Models;

namespace Asolvi.Assignment.Test.Helper.Generators
{
    public static class MockDataGenerator
    {
        private static readonly Random Random = new Random();

        public static List<T> GenerateList<T>(Func<T> generatorFunc, int numberOfItems)
        {
            var list = new List<T>();
            for (var c = 0; c < numberOfItems; c++)
            {
                list.Add(generatorFunc());
            }
            return list;
        }

        public static Event GenerateEvent()
        {
            var id = Guid.NewGuid();
            return new Event()
            {
                Id = id,
                Address = $"Address-{id:N}",
                Description = $"Description-{id:N}",
                TicketPrice = Random.Next(int.MinValue, int.MaxValue),
                Title = $"Title-{id:N}",
                Date = GenerateDateTime()
            };
        }

        public static EventDto GenerateEventDto()
        {
            var id = Guid.NewGuid();
            return new EventDto()
            {
                Id = id,
                Address = $"Address-{id:N}",
                Description = $"Description-{id:N}",
                TicketPrice = Random.Next(int.MinValue, int.MaxValue),
                Title = $"Title-{id:N}",
                Date = GenerateDateTime()
            };
        }

        public static DateTime GenerateDateTime()
        {
            var year = Random.Next(DateTime.Now.Year - 10, DateTime.Now.Year);
            var month = Random.Next(1, 12);
            var day = Random.Next(1, 28);
            return new DateTime(year, month, day);
        }
    }
}
