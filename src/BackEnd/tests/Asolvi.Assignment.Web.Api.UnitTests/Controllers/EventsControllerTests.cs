﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asolvi.Assignment.Data.Abstractions.Exceptions;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Abstractions.Services.Repositories;
using Asolvi.Assignment.Test.Helper.Generators;
using Asolvi.Assignment.Test.Helper.TestsData;
using Asolvi.Assignment.Web.Api.Controllers;
using Asolvi.Assignment.Web.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Asolvi.Assignment.Web.Api.UnitTests.Controllers
{
    public class EventsControllerTests
    {
        [Fact]
        public void Ctor_WhenNullRepositoryArgument_ShouldThrow()
        {
            Assert.Throws<ArgumentNullException>(() => new EventsController(null));
        }

        [Theory]
        [ClassData(typeof(EventsListDataSource))]
        public async Task Get_ShouldReturnDataFromRepository(IList<Event> events)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.GetAll()).ReturnsAsync(events);

            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Get();
            Assert.IsType<OkObjectResult>(result);
            
            var dataResult = (((OkObjectResult)result).Value as IEnumerable<EventDto>)?.ToList();
            Assert.NotNull(dataResult);
            Assert.Equal(events.Count, dataResult.Count());

            foreach (var @event in events)
            {
                Assert.Contains(dataResult, o => o.Id == @event.Id
                                                && o.Title == @event.Title
                                                && o.Address == @event.Address
                                                && o.Date == @event.Date
                                                && o.Description == @event.Description
                                                && o.TicketPrice == @event.TicketPrice);
            }
        }

        [Theory]
        [ClassData(typeof(GuidDataSource))]
        public async Task Get_WhenEntityNotFoundForId_ShouldReturnNotFound(Guid id)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Get(It.Is((Guid eventId)=> eventId == id))).ReturnsAsync((Event)null);

            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Get(id);
            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [ClassData(typeof(EventDataSource))]
        public async Task Get_WhenEntityFoundForId_ShouldReturnEntity(Event @event)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Get(It.Is((Guid id)=> id == @event.Id))).ReturnsAsync(@event);
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Get(@event.Id);
            Assert.IsType<OkObjectResult>(result);
            
            var eventReturned = ((OkObjectResult)result).Value as EventDto;
            Assert.NotNull(eventReturned);
            Assert.Equal(@event.Id, eventReturned.Id);
            Assert.Equal(@event.Address, eventReturned.Address);
            Assert.Equal(@event.Date, eventReturned.Date);
            Assert.Equal(@event.Description, eventReturned.Description);
            Assert.Equal(@event.TicketPrice, eventReturned.TicketPrice);
            Assert.Equal(@event.Title, eventReturned.Title);
        }


        [Fact]
        public async Task Post_WhenModelStateContainsError_ShouldReturnBadRequest()
        {
            var mockRepository = new Mock<IEventsRepository>();
            var sut = new EventsController(mockRepository.Object);
            sut.ModelState.AddModelError("Error","Error");
            var result = await sut.Post(Mock.Of<EventDto>());
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Single((SerializableError)((BadRequestObjectResult)result).Value);
        }

        [Theory]
        [ClassData(typeof(EventDtoDataSource))]
        public async Task Post_WhenEventCreated_ShouldReturnCreated(EventDto @event)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Add(It.IsAny<Event>())).Returns(Task.CompletedTask);
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Post(@event);
            Assert.IsType<CreatedAtActionResult>(result);

            var createdResult = (CreatedAtActionResult) result;
            Assert.Equal("Get", createdResult.ActionName);
            Assert.Contains("id", createdResult.RouteValues.Keys);
            Assert.NotNull(createdResult.RouteValues["id"]);
            Assert.NotNull(createdResult.Value);

            var eventCreated = (EventDto) createdResult.Value;
            Assert.Equal(@event.Address, eventCreated.Address);
            Assert.Equal(@event.Date, eventCreated.Date);
            Assert.Equal(@event.Description, eventCreated.Description);
            Assert.Equal(@event.TicketPrice, eventCreated.TicketPrice);
            Assert.Equal(@event.Title, eventCreated.Title);
        }

        [Fact]
        public async Task Put_WhenModelStateContainsError_ShouldReturnBadRequest()
        {
            var mockRepository = new Mock<IEventsRepository>();
            var sut = new EventsController(mockRepository.Object);
            sut.ModelState.AddModelError("Error", "Error");
            var result = await sut.Put(Guid.NewGuid(), Mock.Of<EventDto>());
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Single((SerializableError)((BadRequestObjectResult)result).Value);
        }

        [Theory]
        [ClassData(typeof(GuidAndEventDtoDataSource))]
        public async Task Put_WhenEventUpdated_ShouldReturnNoContent(Guid id, EventDto @event)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Update(It.IsAny<Event>())).Returns(Task.CompletedTask);
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Put(id, @event);
            Assert.IsType<NoContentResult>(result);
        }

        [Theory]
        [ClassData(typeof(GuidAndEventDtoDataSource))]
        public async Task Put_WhenEventNotFound_ShouldReturnNotFound(Guid id, EventDto @event)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Update(It.IsAny<Event>())).ThrowsAsync(new EntityNotFoundException(id, typeof(Event)));
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Put(id, @event);
            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [ClassData(typeof(GuidDataSource))]
        public async Task Delete_WhenEventNotFound_ShouldReturnNotFound(Guid id)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Delete(It.Is((Guid eventId)=> eventId == id))).ThrowsAsync(new EntityNotFoundException(id, typeof(Event)));
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Delete(id);
            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [ClassData(typeof(GuidDataSource))]
        public async Task Delete_WhenEventDeleted_ShouldReturnNoContent(Guid id)
        {
            var mockRepository = new Mock<IEventsRepository>();
            mockRepository.Setup(o => o.Delete(It.Is((Guid eventId) => eventId == id))).Returns(Task.CompletedTask);
            var sut = new EventsController(mockRepository.Object);
            var result = await sut.Delete(id);
            Assert.IsType<NoContentResult>(result);
        }
    }
}
