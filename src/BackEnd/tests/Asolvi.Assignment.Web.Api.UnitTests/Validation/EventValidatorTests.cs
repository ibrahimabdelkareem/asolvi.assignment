﻿using System;
using System.Collections.Generic;
using System.Text;
using Asolvi.Assignment.Test.Helper.TestsData;
using Asolvi.Assignment.Web.Api.Models;
using Asolvi.Assignment.Web.Api.Validation;
using Xunit;

namespace Asolvi.Assignment.Web.Api.UnitTests.Validation
{
    public class EventValidatorTests
    {
        [Theory]
        [ClassData(typeof(EventDtoDataSource))]
        public void Validate_WhenTitleIsNullOrEmpty_ShouldReturnInvalid(EventDto @event)
        {
            var sut = new EventValidator();
            @event.Title = null;
            Assert.False(sut.Validate(@event).IsValid);

            @event.Title = string.Empty;
            Assert.False(sut.Validate(@event).IsValid);
        }

        [Theory]
        [ClassData(typeof(EventDtoDataSource))]
        public void Validate_WhenAddressIsNullOrEmpty_ShouldReturnInvalid(EventDto @event)
        {
            var sut = new EventValidator();
            @event.Address = null;
            Assert.False(sut.Validate(@event).IsValid);

            @event.Address = string.Empty;
            Assert.False(sut.Validate(@event).IsValid);
        }

        [Theory]
        [ClassData(typeof(EventDtoDataSource))]
        public void Validate_WhenDateIsLessThanUtcToday_ShouldReturnInvalid(EventDto @event)
        {
            var sut = new EventValidator();
            @event.Date = DateTime.UtcNow.AddDays(-1);
            Assert.False(sut.Validate(@event).IsValid);
        }

        [Theory]
        [ClassData(typeof(EventDtoDataSource))]
        public void Validate_WhenTicketPriceIsLessThanZero_ShouldReturnInvalid(EventDto @event)
        {
            var sut = new EventValidator();
            @event.TicketPrice = -1;
            Assert.False(sut.Validate(@event).IsValid);
        }
    }
}
