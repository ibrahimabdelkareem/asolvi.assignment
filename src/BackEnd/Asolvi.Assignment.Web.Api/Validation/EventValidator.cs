﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asolvi.Assignment.Web.Api.Models;
using FluentValidation;

namespace Asolvi.Assignment.Web.Api.Validation
{
    /// <summary>
    /// Provide validation rules to EventDTO
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{Asolvi.Assignment.Web.Api.Models.EventDto}" />
    public class EventValidator : AbstractValidator<EventDto>
    {
        public EventValidator()
        {
            RuleFor(item => item.Title)
                .NotEmpty();

            RuleFor(item => item.Address)
                .NotEmpty();

            RuleFor(o => o.Date)
                .NotEmpty()
                .GreaterThanOrEqualTo(DateTime.UtcNow);
            
            When(item=> item.TicketPrice.HasValue, ()=> RuleFor(item=> item.TicketPrice).GreaterThan(0));
        }
    }
}
