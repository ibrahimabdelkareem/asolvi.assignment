﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asolvi.Assignment.Web.Api.Models
{
    public class EventDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public DateTime Date { get; set; }

        public double? TicketPrice { get; set; }

    }
}
