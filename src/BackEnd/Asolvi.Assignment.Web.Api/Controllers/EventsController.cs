﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asolvi.Assignment.Data.Abstractions.Exceptions;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Abstractions.Services;
using Asolvi.Assignment.Data.Abstractions.Services.Repositories;
using Asolvi.Assignment.Web.Api.Models;
using Mapster;
using Microsoft.AspNetCore.Mvc;

namespace Asolvi.Assignment.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {
        private readonly IEventsRepository _repository;

        public EventsController(IEventsRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = await _repository.GetAll();
            return Ok(data.Select(o=> o.Adapt<EventDto>()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var data = await _repository.Get(id);
            if (data == null)
                return NotFound();

            return Ok(data.Adapt<EventDto>());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]EventDto item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var model = item.Adapt<Event>();
            await _repository.Add(model);
            item = model.Adapt<EventDto>();
            return CreatedAtAction(nameof(Get), new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody]EventDto item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                item.Id = id;
                var model = item.Adapt<Event>();
                await _repository.Update(model);
                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await _repository.Delete(id);
                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
