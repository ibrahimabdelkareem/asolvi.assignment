﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Abstractions.Services.Repositories;

namespace Asolvi.Assignment.Data.InMemory.Services.Repositories
{
    /// <summary>
    /// Repository To Store Data In Memory
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Abstractions.Services.Repositories.IEventsRepository" />
    public class InMemoryEventsRepository : IEventsRepository
    {
        private readonly ConcurrentDictionary<Guid, Event> _cache;

        public InMemoryEventsRepository(ConcurrentDictionary<Guid,Event> cache)
        {
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        public InMemoryEventsRepository()
        :this(new ConcurrentDictionary<Guid, Event>())
        {
        }

        public Task<IList<Event>> GetAll()
        {
            return Task.FromResult((IList<Event>) _cache.Values.ToList());
        }

        public Task<Event> Get(Guid id)
        {
            _cache.TryGetValue(id, out var item);
            return Task.FromResult(item);
        }

        public Task Add(Event item)
        {
            _cache.TryAdd(item.Id, item);
            return Task.CompletedTask;
        }

        public Task Update(Event item)
        {
            if (_cache.TryGetValue(item.Id, out var oldItem))
            {
                _cache.TryUpdate(item.Id, item, oldItem);
            }
            
            return Task.CompletedTask;
        }

        public Task Delete(Guid id)
        {
            _cache.TryRemove(id, out _);
            return Task.CompletedTask;
        }
    }
}