﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services
{
    public interface IJsonFileReaderWriter
    {
        Task<JArray> Read(string filePath);

        Task Write(string filePath, JArray array);
    }
}