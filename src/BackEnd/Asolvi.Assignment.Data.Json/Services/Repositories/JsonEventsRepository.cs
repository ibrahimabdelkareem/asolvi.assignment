﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Asolvi.Assignment.Data.Abstractions.Exceptions;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Abstractions.Services;
using Asolvi.Assignment.Data.Abstractions.Services.Repositories;
using Asolvi.Assignment.Data.Json.Services.JsonObjects;
using Asolvi.Assignment.Data.Json.Services.Resolvers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services.Repositories
{
    /// <summary>
    /// Perform CRUD functions on json files
    /// It initialize InMemoryEventsRepository by seeking it with all the data in json file to speed up reading
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Abstractions.Services.Repositories.IEventsRepository" />
    /// <seealso cref="System.IDisposable" />
    public class JsonEventsRepository : IEventsRepository, IDisposable
    {
        private readonly SemaphoreSlim _repositoryInitializationLock;
        private readonly IEventsRepository _cache;
        private readonly IEntityJsonFilePathResolver _filePathResolver;
        private readonly IJsonFileReaderWriter _jsonFileReaderWriter;
        private readonly IJsonObjectsRefFactory _jsonObjectsRefFactory;
        private IEventsJsonObjectsRef _jsonObjectsRefs;
        private string _filePath;
        private bool _repositoryInitialized;

        public JsonEventsRepository(IEventsRepository cache,
            IEntityJsonFilePathResolver filePathResolver,
            IJsonFileReaderWriter jsonFileReaderWriter,
            IJsonObjectsRefFactory jsonObjectsRefFactory)
        {
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
            _filePathResolver = filePathResolver ?? throw new ArgumentNullException(nameof(cache));
            _jsonFileReaderWriter = jsonFileReaderWriter ?? throw new ArgumentNullException(nameof(jsonFileReaderWriter));
            _jsonObjectsRefFactory = jsonObjectsRefFactory ?? throw new ArgumentNullException(nameof(jsonObjectsRefFactory));
            _repositoryInitializationLock = new SemaphoreSlim(1, 1);
        }

        private string FilePath => _filePath ?? (_filePath = _filePathResolver.Resolve(typeof(Event)));

        public async Task<IList<Event>> GetAll()
        {
            await EnsureInitialized();
            return await _cache.GetAll();
        }


        public async Task<Event> Get(Guid id)
        {
            if (id == default(Guid))
                throw new ArgumentNullException(nameof(id));

            await EnsureInitialized();
            return await _cache.Get(id);
        }

        public async Task Add(Event @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            await EnsureInitialized();
            @event.Id = Guid.NewGuid();
            @event.Created = DateTime.UtcNow;
            _jsonObjectsRefs.AddJsonObject(@event);
            await _jsonFileReaderWriter.Write(FilePath, _jsonObjectsRefs.JsonArrayRef);
            await _cache.Add(@event);
        }

        public async Task Update(Event @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            await EnsureInitialized();
            var currentEvent = await _cache.Get(@event.Id);
            if (currentEvent == null)
                throw new EntityNotFoundException(@event.Id, typeof(Event));

            @event = AdaptUpdatedValues(currentEvent, @event);
            _jsonObjectsRefs.UpdateJsonObject(@event);
            await _jsonFileReaderWriter.Write(FilePath, _jsonObjectsRefs.JsonArrayRef);
            await _cache.Update(@event);
        }

        public async Task Delete(Guid id)
        {
            if (id == default(Guid))
                throw new ArgumentNullException(nameof(id));

            await EnsureInitialized();
            if (await _cache.Get(id) == null)
                throw new EntityNotFoundException(id, typeof(Event));
            
            _jsonObjectsRefs.DeleteJsonObject(id);
            await _jsonFileReaderWriter.Write(FilePath, _jsonObjectsRefs.JsonArrayRef);
            await _cache.Delete(id);
        }

        private async Task EnsureInitialized()
        {
            if (_repositoryInitialized)
                return;

            try
            {
                await _repositoryInitializationLock.WaitAsync();

                if (_repositoryInitialized)
                    return;

                var jsonArray = await _jsonFileReaderWriter.Read(FilePath);
                _jsonObjectsRefs = _jsonObjectsRefFactory.Create(jsonArray);

                foreach (var jsonObject in jsonArray)
                {
                    var obj = jsonObject.ToObject<Event>();
                    await _cache.Add(obj);
                }

                _repositoryInitialized = true;
            }
            finally
            {
                _repositoryInitializationLock.Release();
            }
        }

        private Event AdaptUpdatedValues(Event currentEvent, Event @event)
        {
            currentEvent.Address = @event.Address;
            currentEvent.Date = @event.Date;
            currentEvent.Description = @event.Description;
            currentEvent.TicketPrice = @event.TicketPrice;
            currentEvent.Title = @event.Title;
            currentEvent.Updated = DateTime.UtcNow;
            return currentEvent;
        }


        public void Dispose()
        {
            _repositoryInitializationLock?.Dispose();
        }
    }
}
