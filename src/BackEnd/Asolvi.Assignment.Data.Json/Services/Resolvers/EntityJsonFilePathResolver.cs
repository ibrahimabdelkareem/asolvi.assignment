﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Asolvi.Assignment.Data.Json.Services.Resolvers
{
    /// <summary>
    /// Resolve the json data file path of a type
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Json.Services.Resolvers.IEntityJsonFilePathResolver" />
    public class EntityJsonFilePathResolver : IEntityJsonFilePathResolver
    {
        private readonly string _folderPath;

        public EntityJsonFilePathResolver(string folderPath)
        {
            if(string.IsNullOrEmpty(folderPath))
                throw new ArgumentNullException(folderPath);

            _folderPath = folderPath;
        }

        public string Resolve(Type type)
        {
            if(type == null)
                throw new ArgumentNullException(nameof(type));

            return Path.Combine(_folderPath, $"{type.Name}.json");
        }
    }
}
