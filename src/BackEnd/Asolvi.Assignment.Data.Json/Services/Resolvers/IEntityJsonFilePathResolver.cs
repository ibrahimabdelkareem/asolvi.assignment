﻿using System;

namespace Asolvi.Assignment.Data.Json.Services.Resolvers
{
    public interface IEntityJsonFilePathResolver
    {
        string Resolve(Type type);
    }
}