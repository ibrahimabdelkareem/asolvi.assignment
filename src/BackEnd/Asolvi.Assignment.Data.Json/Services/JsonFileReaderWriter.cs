﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services
{
    /// <summary>
    /// Provide the functions to read/write data from json file while synchronizing access to those files
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Json.Services.IJsonFileReaderWriter" />
    /// <seealso cref="System.IDisposable" />
    public class JsonFileReaderWriter : IJsonFileReaderWriter, IDisposable
    {
        private static readonly ConcurrentDictionary<string, SemaphoreSlim> Semaphores = new ConcurrentDictionary<string, SemaphoreSlim>();
        
        public async Task<JArray> Read(string filePath)
        {
            FileStream fileStream = null;
            StreamReader streamWriter = null;
            JsonReader jsonReader = null;
            var fileSyncSemaphore = GetSemaphore(filePath);
            try
            {
                await fileSyncSemaphore.WaitAsync();

                if (!File.Exists(filePath))
                {
                    return new JArray();
                }

                fileStream = File.OpenRead(filePath);
                streamWriter = new StreamReader(fileStream);
                jsonReader = new JsonTextReader(streamWriter);
                return (JArray)await JArray.ReadFromAsync(jsonReader);
            }
            finally
            {
                jsonReader?.Close();
                streamWriter?.Dispose();
                fileStream?.Close();
                fileStream?.Dispose();
                fileSyncSemaphore?.Release();
            }
        }

        public async Task Write(string filePath, JArray array)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            var fileSyncSemaphore = GetSemaphore(filePath);
            try
            {
                await fileSyncSemaphore.WaitAsync();
                var fileInfo = new FileInfo(filePath);
                if (!fileInfo.Directory.Exists)
                    fileInfo.Directory.Create();
                fileStream = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                streamWriter = new StreamWriter(fileStream, Encoding.UTF8);
                streamWriter.Write(JsonConvert.SerializeObject(array));
            }
            finally
            {
                streamWriter?.Dispose();
                fileStream?.Close();
                fileStream?.Dispose();
                fileSyncSemaphore.Release();
            }
        }

        private SemaphoreSlim GetSemaphore(string filePath)
        {
            return Semaphores.GetOrAdd(filePath, new SemaphoreSlim(1, 1));
        }

        public void Dispose()
        {
            foreach(var semaphore in Semaphores)
                semaphore.Value.Dispose();
        }
    }
}
