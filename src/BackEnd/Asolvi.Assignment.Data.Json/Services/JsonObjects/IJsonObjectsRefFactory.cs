﻿using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services.JsonObjects
{
    public interface IJsonObjectsRefFactory
    {
        IEventsJsonObjectsRef Create(JArray jsonArray);
    }
}