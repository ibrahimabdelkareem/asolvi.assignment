﻿using System;
using Asolvi.Assignment.Data.Abstractions.Models;
using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services.JsonObjects
{
    public interface IEventsJsonObjectsRef
    {
        JArray JsonArrayRef { get; }

        void AddJsonObject(Event @event);

        void UpdateJsonObject(Event @event);

        void DeleteJsonObject(Guid id);
    }
}