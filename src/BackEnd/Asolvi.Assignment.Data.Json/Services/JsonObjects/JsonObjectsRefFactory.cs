﻿using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services.JsonObjects
{
    /// <summary>
    /// Factory to create IEventsJsonObjectsRef
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Json.Services.JsonObjects.IJsonObjectsRefFactory" />
    public class JsonObjectsRefFactory : IJsonObjectsRefFactory
    {
        public IEventsJsonObjectsRef Create(JArray jsonArray)
        {
            return new EventsJsonObjectsRef(jsonArray);
        }
    }
}