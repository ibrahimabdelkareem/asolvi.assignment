﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Asolvi.Assignment.Data.Abstractions.Models;
using Newtonsoft.Json.Linq;

namespace Asolvi.Assignment.Data.Json.Services.JsonObjects
{
    /// <summary>
    /// Provide and store caches to JsonArray/JsonObjects to speed up the CRUD operations
    /// </summary>
    /// <seealso cref="Asolvi.Assignment.Data.Json.Services.JsonObjects.IEventsJsonObjectsRef" />
    public class EventsJsonObjectsRef : IEventsJsonObjectsRef
    {

        private readonly ConcurrentDictionary<Guid, JObject> _jsonObjectsRef;
        public EventsJsonObjectsRef(JArray jsonArray)
        {
            JsonArrayRef = jsonArray ?? throw new ArgumentNullException(nameof(jsonArray));
            _jsonObjectsRef = new ConcurrentDictionary<Guid, JObject>();
            InitializeJsonObjectsRefs();
        }

        public JArray JsonArrayRef { get; }

        public void AddJsonObject(Event @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            var jsonObject = JObject.FromObject(@event);
            JsonArrayRef.Add(jsonObject);
            _jsonObjectsRef.TryAdd(@event.Id, jsonObject);
        }

        public void UpdateJsonObject(Event @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));
            if (!_jsonObjectsRef.TryGetValue(@event.Id, out var jsonObject))
                return;

            jsonObject.Property(nameof(Event.Address));
            jsonObject[nameof(Event.Address)] = @event.Address == null ? null : JToken.FromObject(@event.Address);
            jsonObject[nameof(Event.Date)] = JToken.FromObject(@event.Date);
            jsonObject[nameof(Event.Description)] = @event.Description == null ? null : JToken.FromObject(@event.Description);
            jsonObject[nameof(Event.TicketPrice)] = @event.TicketPrice.HasValue ? JToken.FromObject(@event.TicketPrice.Value) : null;
            jsonObject[nameof(Event.Title)] = @event.Title == null ? null : JToken.FromObject(@event.Title);
            jsonObject[nameof(Event.Updated)] = JToken.FromObject(@event.Updated);
        }

        public void DeleteJsonObject(Guid id)
        {
            if (!_jsonObjectsRef.TryRemove(id, out var jsonObject))
                return;

            JsonArrayRef.Remove(jsonObject);
        }

        private void InitializeJsonObjectsRefs()
        {
            foreach (JObject jsonObject in JsonArrayRef)
            {
                _jsonObjectsRef.TryAdd(Guid.Parse(jsonObject["Id"].Value<string>()), jsonObject);
            }
        }
    }
}
