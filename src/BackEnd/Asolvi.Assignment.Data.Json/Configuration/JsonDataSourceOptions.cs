﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Asolvi.Assignment.Data.Json.Configuration
{
    public class JsonDataSourceOptions
    {
        public string DataFolder { get; set; }
    }
}
