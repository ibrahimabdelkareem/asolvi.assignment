﻿using System;
using System.Collections.Generic;
using System.Text;
using Asolvi.Assignment.Data.Abstractions.Models;
using Asolvi.Assignment.Data.Abstractions.Services;
using Asolvi.Assignment.Data.Abstractions.Services.Repositories;
using Asolvi.Assignment.Data.InMemory.Services.Repositories;
using Asolvi.Assignment.Data.Json.Configuration;
using Asolvi.Assignment.Data.Json.Services;
using Asolvi.Assignment.Data.Json.Services.JsonObjects;
using Asolvi.Assignment.Data.Json.Services.Repositories;
using Asolvi.Assignment.Data.Json.Services.Resolvers;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Registers all the services neccessary to work with JsonEventsRepository
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        public static void AddJsonDataSource(this IServiceCollection serviceCollection, Action<JsonDataSourceOptions> optionsInitializer)
        {
            var options = new JsonDataSourceOptions();
            optionsInitializer(options);
            if (string.IsNullOrEmpty(options.DataFolder))
                throw new InvalidOperationException();

            serviceCollection.AddSingleton<IJsonFileReaderWriter, JsonFileReaderWriter>();
            serviceCollection.AddSingleton<IJsonObjectsRefFactory, JsonObjectsRefFactory>();
            serviceCollection.AddSingleton<IEventsRepository, JsonEventsRepository>(serviceProvider =>
            new JsonEventsRepository(new InMemoryEventsRepository(), 
                serviceProvider.GetRequiredService<IEntityJsonFilePathResolver>(),
                serviceProvider.GetRequiredService<IJsonFileReaderWriter>(),
                serviceProvider.GetRequiredService<IJsonObjectsRefFactory>()));

            serviceCollection.AddSingleton<IEntityJsonFilePathResolver, EntityJsonFilePathResolver>(serviceProvider => new EntityJsonFilePathResolver(options.DataFolder));
        }
    }
}
