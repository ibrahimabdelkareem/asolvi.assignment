﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asolvi.Assignment.Data.Abstractions.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(object entityId, Type entityType)
        {
            EntityId = entityId;
            EntityType = entityType;
        }

        public object EntityId { get; }

        public Type EntityType { get; }
    }
}
