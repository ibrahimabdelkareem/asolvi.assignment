﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asolvi.Assignment.Data.Abstractions.Models;

namespace Asolvi.Assignment.Data.Abstractions.Services.Repositories
{
    public interface IEventsRepository
    {
        Task<IList<Event>> GetAll();

        Task<Event> Get(Guid key);

        Task Add(Event item);

        Task Update(Event item);

        Task Delete(Guid id);
    }
}
