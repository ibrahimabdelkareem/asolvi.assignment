﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Asolvi.Assignment.Data.Abstractions.Models
{
    public class Event
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public DateTime Date { get; set; }

        public double? TicketPrice { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }
        
    }
}
